# -*- coding: utf-8 -*-
__authors__="Jes\xc3\xbas Zerpa"
from urllib import urlopen
from HTMLParser import HTMLParser
def encode(html):
	return html.replace("\xc3\xa1","&aacute;").replace("\xc3\xa8","&eacute;").replace("\xc3\xad","&iacute;").replace("\xc3\xb3","&oacute").replace("\xc3\xba","&uacute").replace("\xc3\xb1","&ntilde")
def decode(html):
	return html.replace("&aacute;","\xc3\xa1").replace("&eacute;","\xc3\xa8").replace("&iacute;","\xc3\xad").replace("&oacute","\xc3\xb3").replace("&uacute","\xc3\xba").replace("&ntilde","\xc3\xb1")
class  Selector(object):
	"""docstring for  Selector"""
	def __init__(self,entrada):
		self.clases=[]
		self.selector=entrada
		if entrada.strip().startswith("."):
			self.clases=entrada.split(".")
		self.element=None
		self.attrs={}
		self._id=None
		self.tags=["head",
			"title",
			"base",
			"meta",
			"style",
			"script",
			"noscript",
			"body",
			"section",
			"nav",
			"article",
			"aside",
			"h1",
			"h2",
			"h3",
			"h4",
			"h5",
			"h6",
			"header",
			"footer",
			"address",
			"main",
			"p",
			"hr",
			"pre",
			"blockquote",
			"ol",
			"ul",
			"li",
			"dl",
			"dt",
			"dd",
			"figure",
			"figcaption",
			"div",
			"a",
			"em",
			"strong",
			"small",
			"s",
			"cite",
			"q",
			"dfn",
			"abbr",
			"data",
			"time",
			"code",
			"var",
			"samp",
			"kdb",
			"sub",
			"sup",
			"i",
			"b",
			"u",
			"mark",
			"dbi",
			"dbo",
			"span",
			"br",
			"wbr",
			"ins",
			"del",
			"img",
			"iframe",
			"embed",
			"param",
			"video",
			"audio",
			"source",
			"track",
			"canvas",
			"map",
			"area",
			"svg",
			"math",
			"table",
			"caption",
			"colgroup",
			"col",
			"tr",
			"td",
			"th",
			"form",
			"fieldset",
			"legend",
			"label",
			"input",
			"button",
			"select",
			"datalist",
			"option",
			"textarea",
			]
		self.tags.sort(key=len,reverse=True)
		for elem in self.tags:
			if entrada.startswith(elem):
				self.element=elem
				break
		if "#" in entrada:
			self._id=entrada[entrada.find("#")+1:]
		if "[" in entrada and "]" in entrada:
			i=entrada.find("[")
			f=entrada.find("]",i)
			att,value=entrada[i-1:f-1].split("=")

			if att.endswith("~"):
				pass
			if att.endswith("|"):
				pass
			self.attrs[att]=value
	def isNode(self,node):
		
		notclass=False
		for elem in self.clases:
			if elem not in node.classList.value:
				notclass=True
		if not notclass:
			
			for elem in self.attrs:

				if self.attrs[elem]!=node.atributtes[elem]:
					return False

			else:

				if node.tagName!=self.element and self.element!=None:
					return False
				else:
					return True
		return False
	def __str__(self):
		return self.__class__.__name__+"("+self.selector+")"




class DOMTokenList(object):
	def __init__(self,clases=[]):
		self.value=clases
		pass

	def __getitem__(self,clave):
		return self.value[clave]
	def __setitem__(self,clave,valor):
		self.value[clave]=valor
	def __contains__(self,obj):
		return obj in self.value
	def __iter__(self):
		self.c=0
		return self
	def next(self):
		if self.c<len(self.value):
			x=self.c
			self.c+=1
			return self.value[x]

		else:
			
			raise StopIteration
	def __str__(self):
		return self.__class__.__name__+str(self.value)
		
class NamedNodeMap(object):
	def __init__(self,attrs={}):
		self.value=attrs
		pass

	def __getitem__(self,clave):
		if clave in self.value and self.value[clave]!=None:
			return self.value[clave].encode("utf-8")
	def __setitem__(self,clave,valor):
		self.value[clave]=valor
	def __contains__(self,obj):
		return obj in self.value
	def __iter__(self):
		self.c=0
		return self
	def next(self):
		if self.c<len(self.value):
			x=self.c
			self.c+=1
			return list(self.value)[x]

		else:
			
			raise StopIteration
	def __str__(self):

		return self.__class__.__name__+str(list(self.value))
class HTMLNode(object):
	"""docstring for HTMLNode"""
	def __init__(self,nodeName=None,attrs={}):
		super(HTMLNode, self).__init__()
		self.nodeName =nodeName
		self.tagName=self.nodeName
		self.nodeValue=None
		self.nodeType=None#(int) identifica al tipo de nodo
		self.parentNode=None
		self.parentElement=self.parentNode
		self.childNodes=[]
		self.firstChild=None
		self.lastChild=None
		self.previousSibling=None
		self.nextSibling=None
		self.attributes=None
		self.ownerDocument=None
		self.atributtes=NamedNodeMap(attrs)
		self._innerHTML=[]
		self.classList=DOMTokenList(attrs["class"].split(" ") if "class" in attrs else [])
		self.data={}
		
		self.children=[]

	@property
	def outerHTML(self):
		attrs=""
		for elem in self.atributtes:
			if self.atributtes[elem]!=None:
				attrs+=" "+elem+"='"+self.atributtes[elem].replace("'","\"")+"'"

		html="<"+self.tagName+attrs+">"+"".join([x if type(x)==str else x.outerHTML for x in self._innerHTML])+"</"+self.tagName+">"
		return encode(html)

	@outerHTML.setter
	def outerHTML(self,html):
		nodo=pyQuery(html).document.documentElement
		self._innerHTML=nodo._innerHTML
		self.childNodes=nodo.childNodes
		self.tagName=nodo.tagName
		self.atributtes=nodo.atributtes
		self.classList=nodo.classList


	@property
	def innerHTML(self):
		html=""
		for elem in self._innerHTML:
			if type(elem)==HTMLNode:
				html+=elem.outerHTML
			else:
				html+=elem
		
		return html
	@innerHTML.setter
	def innerHTML(self,html):
		self.childNodes=[]
		self._innerHTML=[]
		
		
		for elem in pyQuery(html).set:

			self.childNodes.append(elem)
			self._innerHTML.append(elem)
		
		

	@property
	def innerText(self):
		text=""
		for elem in self._innerHTML:
			if type(elem)==HTMLNode:
				text+=elem.innerText
			else:
				text+=elem
		
		return text

	




	def getElementByTagName(self):
		pass
	def insertBefore(self,nodo):
		i=self.parentNode.children.index(self)
		nodo.previousSibling=self.children[i-1]
		nodo.nextSibling=self.children[i]
		self.children[i].previousSibling=nodo
		self.children.insert(i,nodo)
	def replaceChild(self,old,new):
		self.children[old]=new
	def removeChild(self,nodo):
		if nodo in self.children:
			self.children.remove(node)
			self._innerHTML.remove(node)
	def hasChildNodes(self):
		if self.children!=[]:
			return True
		return False

	def appendChild(self,node):
		node.parentNode=self

		if len(self.children)>0:
			node.previousSibling=self.children[-1]
			self.children[-1].nextSibling=node
		self.children.append(node)
		self._innerHTML.append(node)

	def cloneNode(self):
		from copy import copy 
		return copy(self)

	def __str__(self):
		_id=""
		if "id" in self.atributtes:
			_id="#"+self.atributtes["id"]
			print self.atributtes
		
		return self.nodeName.upper()+_id

class pyQuery(object):
	"""docstring for pyQuery"""
	def __init__(self,html):
		super(pyQuery, self).__init__()
		self.parser = Scraper()
		self.parser.feed(decode(html))

		self.tags=["head",
			"title",
			"base",
			"meta",
			"style",
			"script",
			"noscript",
			"body",
			"section",
			"nav",
			"article",
			"aside",
			"h1",
			"h2",
			"h3",
			"h4",
			"h5",
			"h6",
			"header",
			"footer",
			"address",
			"main",
			"p",
			"hr",
			"pre",
			"blockquote",
			"ol",
			"ul",
			"li",
			"dl",
			"dt",
			"dd",
			"figure",
			"figcaption",
			"div",
			"a",
			"em",
			"strong",
			"small",
			"s",
			"cite",
			"q",
			"dfn",
			"abbr",
			"data",
			"time",
			"code",
			"var",
			"samp",
			"kdb",
			"sub",
			"sup",
			"i",
			"b",
			"u",
			"mark",
			"dbi",
			"dbo",
			"span",
			"br",
			"wbr",
			"ins",
			"del",
			"img",
			"iframe",
			"embed",
			"param",
			"video",
			"audio",
			"source",
			"track",
			"canvas",
			"map",
			"area",
			"svg",
			"math",
			"table",
			"caption",
			"colgroup",
			"col",
			"tr",
			"td",
			"th",
			"form",
			"fieldset",
			"legend",
			"label",
			"input",
			"button",
			"select",
			"datalist",
			"option",
			"textarea",
			]
		self.tags.sort(key=len,reverse=True)
		self.set=[]
		self.document=self.parser.document
	def find(self,selector):

		select=self.__getselector(selector)

		def find(obj,selector,self=self):			
			
			for hijo in obj.children:
				
				
				if selector[0].isNode(hijo):


					if len(selector)>1:
						find(hijo,selector[1:])
					else:
						self.set.append(hijo)
				else:
					find(hijo,selector)


					
						
		
		find(self.document,select)
		
		return self
	def append(self,node):
		self.set[0].appendChild(node)

		
	def __getselector(self,selector):
		

		selectores=[Selector(elem) for elem in selector.split(" ")]
		
		"""
		if selector.strip().startswith(".") and " " not in selector:
			selectores.append(select)
		elif selector.strip().startswith("#") and " " not in selector:

		else:

			for elem in self.tags:
		 		if selector.strip().startswith(elem)
		 			for elem2 in selector.split(" "):
		 				for clase in elem2.split("."):
		 					for _id in clase.split("#"):
		 						if len(_id)>1:
		 							pass



		 			if "." in selector:pass
		 			elif "#" in selector:pass
		 			elif "[" in selector and "]" in selector:pass
		 			elif " " selector:pass
		 			break
		"""
		return selectores
	def __getitem__(self,indice):
		return self.set[indice]
	def __iter__(self):
		self.c=0
		return self
	def __len__(self):
		return len(self.set)
	def next(self):
		if self.c<len(self.set):
			x=self.c
			self.c+=1
			return self.set[x]

		else:
			raise StopIteration
	def eq(self,indice):
		return self(self.set[indice])
	def html(self,html=None):
		for elem in self.set:
			
			elem.innerHTML=str(html)

	def __call__(self,selector):
		self.set=[]
		if type(selector)==HTMLNode:


			self.document=selector.parentNode
			self.set=[selector]

		elif type(selector)==str and selector.strip().startswith("<") and selector.strip().endswith(">"):
			
			node=pyQuery(selector)

			
			node.set=[node.document.children[0]]
			return node
		else:
			self.find(selector)
		return self
	def __str__(self):
		return self.__class__.__name__

	

			

		
''''
class ELEMENT_NODE(HTMLNode):
	"""docstring for ELEMENT_NODE"""
	def __init__(self, tagName):
		super(ELEMENT_NODE, self).__init__()
		

class ATRIBUTE_NODE(HTMLNode):
	"""docstring for ATRIBUTE_NODE"""
	def __init__(self, tagName):
		super(ATRIBUTE_NODE, self).__init__()
		

class TEXT_NODE(HTMLNode):
	"""docstring for TEXT_NODE"""
	def __init__(self, tagName):
		super(TEXT_NODE, self).__init__()
		

class CDATA_SECTION_NODE(HTMLNode):
	"""docstring for CDATA_SECTION"""
	def __init__(self, tagName):
		super(CDATA_SECTION, self).__init__()
		

class ENTITY_REFERENCE_NODE(HTMLNode):
	"""docstring for ENTITY_REFERENCE_NODE"""
	def __init__(self, tagName):
		super(ENTITY_REFERENCE_NODE, self).__init__()
		

class ENTITY_NODE(HTMLNode):
	"""docstring for ENTITY_NODE"""
	def __init__(self, tagName):
		super(ENTITY_NODE, self).__init__()
		

class PROGRESSING_INSTRUCTION_NODE(HTMLNode):
	"""docstring for PROGRESSING_INSTRUCTION_NODE"""
	def __init__(self, tagName):
		super(PROGRESSING_INSTRUCTION_NODE, self).__init__()
		self.tagName = tagName

class COMMENT_NODE(HTMLNode):
	"""docstring for COMMENT_NODE"""
	def __init__(self, tagName):
		super(COMMENT_NODE, self).__init__()
		
'''
class DOCUMENT_NODE(HTMLNode):
	"""docstring for DOCUMENT_NODE"""
	def __init__(self):
		super(DOCUMENT_NODE, self).__init__()
		
'''
class DOCUMENT_TYPE_NODE(HTMLNode):
	"""docstring for DOCUMENT_TYPE_NODE"""
	def __init__(self, tagName):
		super(DOCUMENT_TYPE_NODE, self).__init__()
		

class DOCUMENT_FRAGMENT_NODE(HTMLNode):
	"""docstring for DOCUMENT_FRAGMENT_NODE"""
	def __init__(self, tagName):
		super(DOCUMENT_FRAGMENT_NODE, self).__init__()
'''		

		
class Location(object):
	pass
from modulos.ztec import zu
class Scraper(HTMLParser):
	in_h5=False
	in_link=False
	
	
	def __init__(self):
		HTMLParser.__init__(self)
		self.node_current=None
		self.document=DOCUMENT_NODE()
		self.document.URL=""
		self.document.cookie=""
		self.document.links=[]
		self.document.nodeName="#document"
		self.document.nodeType=9
		self.document.location=Location()
		self.document.title=""
		self.document.lastModified=zu.DateTime()
		self.document.forms=[]
		self.document.all=[]
		self.document.documentElement=HTMLNode()


	






	def handle_starttag(self,tag,attrs):
		attrs=dict(attrs)
		"""
		if tag=="h4":
			self.in_h4=True
		if tag=="a" and "href" in attrs:
			self.in_link=True
			self.chunks=[]
			self.url=attrs["href"]
		"""

		node=HTMLNode(tag,attrs)
		

		if self.node_current==None:
			self.document.appendChild(node)
			self.document.documentElement=node
			self.document.domain="localhost"
			self.node_current=node



		else:

			self.node_current.appendChild(node)
			self.node_current=node
		if tag=="body":
			self.document.body=node
		if tag=="meta" and "charset" in attrs:
			self.document.charset=attrs["charset"]
		if tag=="a":
			self.document.links.append(node)
			
		if tag=="title":
			self.document.title=node
		
		if tag=="form":
			self.document.forms.append(node)
		if tag=="head":
			self.document.head=node

		if "tags_"+tag in dir(self):
			getattr(self,"tags_"+tag).append(node)
		else:
			setattr(self,"tags_"+tag,[node])
		self.document.all.append(node)
	def handle_data(self,data):
		if self.node_current!=None:
			self.node_current._innerHTML.append(data)	
		
	def handle_endtag(self,tag):
		
		if self.node_current!=None:
			self.node_current=self.node_current.parentNode


